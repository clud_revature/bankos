package bankOperatingSystem;

public interface TransactionDAO {
	
	void createUserTransaction(Integer accNo);
	
	void createAccountTransaction(Integer userNo);
	
	void createJointAccountTransaction(Integer userNo1, Integer userNo2);
		
	void changeBalanceTransaction(Integer userNo, Integer accNo, Double oldBal, Double newBal);
	
	void approveAccountTransaction(Integer userNo, Integer accNo);
	
}