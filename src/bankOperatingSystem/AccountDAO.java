package bankOperatingSystem;

public interface AccountDAO {
	
	void createAccount(Integer phone, Double balance);
	
	void createJointAccount(Integer phone, Integer jointPhone, Double balance);
	
	void displayAccountInfo(Integer accNO);
	
	void displayUserAccountInfo(Integer phone);
	
	Boolean verifyAccountOwnershp(Integer accNo, Integer phone);
	
	Double getAccountBalance(Integer accNo);
	
	void changeBalance(Integer accNo, Double amount);

	void approveAccount(Integer accNo);
	
	Boolean checkApproved(Integer accNo);

}
