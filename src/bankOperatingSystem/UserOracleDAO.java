package bankOperatingSystem;

import java.sql.Connection;	
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;

public class UserOracleDAO implements UserDAO {
	
	private Connection conn = JDBCConnection.getInstance().conn;

	public UserOracleDAO() {
	}

	@Override
	public void createUser(Integer phone, String name, String pass, Integer type) {
		
		String sql = "INSERT INTO USERS (PHONE, NAME, PASSWORD, TYPE) VALUES (?, ?, ?, ?)";
		
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, phone);
			ps.setString(2, name);
			ps.setString(3, pass);
			ps.setInt(4, type);
			
			ps.execute();
			
		} catch (SQLIntegrityConstraintViolationException e) {
			System.out.println("Sorry, phone number already exists. Please login or register again!");
		} catch (SQLException e) {
			System.out.println("Error in UserOracleDAO.");
			e.printStackTrace();
		} 
	}
		

	@Override
	public User getUserByPhone(Integer phone, String password) throws InvalidPasswordException {
		
		String sql = "SELECT * FROM USERS WHERE PHONE=?";
		
		PreparedStatement ps;
		ResultSet rs;
		
		String name = null;
		String dbPassword = null;
		Integer type = null;
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, phone);
			
			rs = ps.executeQuery();
			
			while(rs.next()) {
				System.out.println("Phone: "+rs.getInt(1));
				System.out.println("Name: "+rs.getString(2));
				name = rs.getString(2);
				System.out.println("Password: "+rs.getString(3));
				dbPassword = rs.getString(3);
				System.out.println("Type: "+rs.getInt(4));
				type = rs.getInt(4);
				}
			
		} catch (SQLException e) {
			System.out.println("SQL Error in UserOracleDAO getUserByPhone");
			e.printStackTrace();
		}
		
		if (!password.equals(dbPassword)) {
			throw new InvalidPasswordException();
		}
		
		User us = null;
		
		if (type == 1) {
			us = new User(phone, name, password);
		} else if (type == 2) {
			us = new EmployeeUser(phone, name, password);
		} else if (type == 3) {
			us = new AdminUser(phone, name, password);
		}
		
		return us;
	}

	@Override
	public void getUserInfo(Integer phone) {
		
		String sql = "SELECT * FROM USERS WHERE PHONE=?";
		
		PreparedStatement ps;
		ResultSet rs;
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, phone);
			
			rs = ps.executeQuery();
			
			System.out.println("<--------------->");
			System.out.println("       User");
			System.out.println("<--------------->");
			
			while(rs.next()) {
				System.out.println("Phone: "+rs.getInt(1));
				System.out.println("Name: "+rs.getString(2));
				System.out.println("Password: "+rs.getString(3));
				}
			
		} catch (SQLException e) {
			System.out.println("SQL Error in UserOracleDAO getUserByPhone: Users");
			e.printStackTrace();
		}
		
		String sql2 = "SELECT ID, APPROVED, BALANCE FROM ACCOUNTS WHERE USERPHONE = ? OR JOINTUSER = ?";
		
		PreparedStatement ps2;
		ResultSet rs2;
		
		try {
			ps2 = conn.prepareStatement(sql2);
			ps2.setInt(1, phone);
			ps2.setInt(2, phone);
			
			rs2 = ps2.executeQuery();
			
			System.out.println("<--------------->");
			System.out.println("    Accounts");
			System.out.println("<--------------->");
			
			while(rs2.next()) {
				System.out.println("Account #"+rs2.getInt(1));
				System.out.println("Balance: "+rs2.getDouble(3));
				String approved = "Pending";
				if (rs2.getInt(2) == 1) {
					approved = "Yes";
				}
				System.out.println("Approved: "+ approved);
				}
			
		} catch (SQLException e) {
			System.out.println("SQL Error in UserOracleDAO getUserByPhone:Accounts");
			e.printStackTrace();
		}
	}

	@Override
	public void getAllUsers() {
			
			Statement stmt;
			try {
				stmt = conn.createStatement();
				
				ResultSet rs = stmt.executeQuery("SELECT * FROM USERS");
				
				while(rs.next()) {
					System.out.println("Phone: "+rs.getInt(1));
					System.out.println("Name: "+rs.getString(2));
					System.out.println("Address: "+rs.getString(3));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}

	@Override
	public Boolean verifyUser(Integer userNo, String pass) {
		String sql = "SELECT PASSWORD FROM USERS WHERE PHONE = ?";
		
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, userNo);
			
			ResultSet rs = ps.executeQuery();
						
			while(rs.next()) {
				if (rs.getString(1).equals(pass)) {
					return true;
				}				
			}
	
		} catch (SQLException e) {
			System.out.println("Error in UserOracleDAO: verifyUser.");
			e.printStackTrace();
		} 
		return false;
	}

	@Override
	public void userTransactions(Integer userNo) {
		String sql = "SELECT * FROM TRANSACTIONS WHERE USERNUM = ?";
		
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, userNo);
			
			ResultSet rs = ps.executeQuery();
						
			while(rs.next()) {
				System.out.println(">>");
				System.out.println("Transaction #" + rs.getInt(1));
				System.out.println("Account #" + rs.getInt(3));
				System.out.println("Description: " + rs.getString(4));
				}				
			} catch (SQLException e) {
			System.out.println("Error in UserOracleDAO: userTransactions.");
			e.printStackTrace();
		}
	}
	
	@Override
	public void globalTransactions() {
		String sql = "SELECT * FROM TRANSACTIONS";
		
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
						
			while(rs.next()) {
				System.out.println(">>");
				System.out.println("Transaction #" + rs.getInt(1));
				System.out.println("Account #" + rs.getInt(3));
				System.out.println("Description: " + rs.getString(4));
				}				
			} catch (SQLException e) {
			System.out.println("Error in UserOracleDAO: userTransactions.");
			e.printStackTrace();
		}
	}

	@Override
	public void dropAccount(Integer accNo) {
		
		String sql = "DELETE FROM ACCOUNTS WHERE ID = ?";
		
		PreparedStatement ps;
		try {
			
			ps = conn.prepareStatement(sql);
			ps.setInt(1, accNo);
			
			ps.execute();
			
		} catch (SQLException e) {
			System.out.println("Error in UserOracleDAO: Drop Account");
			e.printStackTrace();
		}
	}


}
