package bankOperatingSystem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

public class AccountOracleDAO implements AccountDAO {
	
	private Connection conn = JDBCConnection.getInstance().conn;
	
	@Override
	public void createAccount(Integer phone, Double balance) {
		
		String sql = "INSERT INTO ACCOUNTS (USERPHONE, APPROVED, BALANCE) VALUES (?, ?, ?)";
				
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, phone);
			ps.setInt(2, 0);
			ps.setDouble(3, balance);
			
			ps.execute();
			
		} catch (SQLIntegrityConstraintViolationException e) {
			System.out.println("Sorry, phone number already exists. Please login or register again!");
		} catch (SQLException e) {
			System.out.println("Error in AccountOracleDAO: createAccount");
			e.printStackTrace();
		} 
	}
	
	@Override
	public void createJointAccount(Integer phone, Integer jointPhone, Double balance) {
		
		String sql = "INSERT INTO ACCOUNTS (USERPHONE, APPROVED, BALANCE, JOINTOPTION, JOINTUSER) VALUES (?, 0, ?, 1, ?)";
		
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, phone);
			ps.setDouble(2, balance);
			ps.setInt(3, jointPhone);
			
			ps.execute();
			
		} catch (SQLException e) {
			System.out.println("Error in AccountOracleDAO: createJointAccount");
			e.printStackTrace();
		} 
	}
	
	@Override
	public void displayAccountInfo(Integer accNo) {
		
		String sql = "SELECT * FROM ACCOUNTS WHERE ID = ?";
		
		PreparedStatement ps;
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, accNo);
			
			ResultSet rs = ps.executeQuery();
						
			while(rs.next()) {
				String accountString = ">> Account #";
				if (rs.getInt(5) == 1) {
					accountString = "Joint Account #";
				}
				System.out.println(accountString+rs.getInt(1));
				System.out.println("User #: "+ rs.getInt(2));
				if (rs.getInt(5) == 1){
					System.out.println("Joint User #: "+rs.getInt(6));
				}
				System.out.println("Balance: "+rs.getDouble(4));
				String approved = "Pending";
				if (rs.getInt(3) == 1) {
					approved = "Yes";
				}
				System.out.println("Approved: "+ approved);
				}
		} catch (SQLException e) {
			System.out.println("Error in AccountOracleDAO: displayAccountInfo");
			e.printStackTrace();
		} 
	}
	
	@Override
	public void displayUserAccountInfo(Integer phone) {
		
		String sql = "SELECT * FROM ACCOUNTS WHERE USERPHONE = ? OR JOINTUSER = ?";
		
		PreparedStatement ps;
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, phone);
			ps.setInt(2, phone);
			
			ResultSet rs = ps.executeQuery();
						
			while(rs.next()) {
				System.out.println(">>");
				String accountString = "Account #";
				if (rs.getInt(5) == 1) {
					accountString = "Joint Account #";
				}
				System.out.println(accountString+rs.getInt(1));
				System.out.println("Balance: "+rs.getDouble(4));
				String approved = "Pending";
				if (rs.getInt(3) == 1) {
					approved = "Yes";
				}
				System.out.println("Approved: "+ approved);
				}
		} catch (SQLException e) {
			System.out.println("Error in AccountOracleDAO: displayUserAccountInfo");
			e.printStackTrace();
		} 
	}
	
	@Override
	public Boolean verifyAccountOwnershp(Integer accNo, Integer phone) {
		
		String sql = "SELECT * FROM ACCOUNTS WHERE ID = ?";
				
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, accNo);
			
			ResultSet rs = ps.executeQuery();
						
			while(rs.next()) {
				// checks if account owner or joint owner... both return true
				if (rs.getInt(2) == phone || rs.getInt(6) == phone) {
					return true;
				}				
			}
	
		} catch (SQLException e) {
			System.out.println("Error in AccountOracleDAO: verifyAccountOwnership.");
			e.printStackTrace();
		} 
		
		return false;
	}
	
	@Override
	public Double getAccountBalance(Integer accNo) {
		String sql = "SELECT BALANCE FROM ACCOUNTS WHERE ID = ?";
		
		PreparedStatement ps;
		Double balance = null;
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, accNo);
			ResultSet rs = ps.executeQuery();
			
			rs.next();
			
			balance = rs.getDouble(1);
			
		} catch (SQLException e) {
			System.out.println("Error in AccountOracleDAO: getAccountBalance.");
			e.printStackTrace();
		} 
		return balance;
	}

	@Override
	public void changeBalance(Integer accNo, Double amount) {
		
		String sql = "UPDATE ACCOUNTS SET BALANCE = ? WHERE ID = ?";
				
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setDouble(1, amount);
			ps.setInt(2, accNo);
			
			ps.execute();
			
		} catch (SQLException e) {
			System.out.println("Error in AccountOracleDAO: changeBalance.");
			e.printStackTrace();
		} 
		
	}

	@Override
	public void approveAccount(Integer accNo) {
		
		String sql = "UPDATE ACCOUNTS SET APPROVED = ? WHERE ID = ?";
		
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, 1);
			ps.setInt(2, accNo);
			
			ps.execute();
			
		} catch (SQLException e) {
			System.out.println("Error in AccountOracleDAO: approveAccount.");
			e.printStackTrace();
		} 
	}

	@Override
	public Boolean checkApproved(Integer accNo) {
		
		String sql = "SELECT * FROM ACCOUNTS WHERE ID = ?";
		
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, accNo);
			
			ResultSet rs = ps.executeQuery();
						
			while(rs.next()) {
				if (rs.getInt(3) == 1) {
					return true;
				}				
			}
	
		} catch (SQLException e) {
			System.out.println("Error in AccountOracleDAO: verifyAccountOwnership.");
			e.printStackTrace();
		} 
		
		// TODO Auto-generated method stub
		return false;
	}

	
	

}
