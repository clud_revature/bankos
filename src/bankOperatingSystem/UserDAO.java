package bankOperatingSystem;

import java.util.List;

public interface UserDAO {
	
	// creating user method
	void createUser(Integer phone, String name, String pass, Integer type);
	
	// main method to create user object
	User getUserByPhone(Integer phone, String password) throws InvalidPasswordException;
	
	// getting user info
	void getUserInfo(Integer phone);

	// listing users ... probably change to list
	void getAllUsers();
	
	Boolean verifyUser(Integer userNo, String pass);
	
	void userTransactions(Integer userNo);
	
	void globalTransactions();
	
	void dropAccount(Integer accNo);
	


}
