package bankOperatingSystem;

import java.sql.*;

public class Initial {

	public static void main(String[] args) {
		
		// welcome message
		System.out.println("Hello and welcome to bankOS!");
		
		System.out.println("--> Connecting to Database...");
		
		JDBCConnection db = JDBCConnection.getInstance();
		
		Boolean first = true;
		String adminName = null;
		
		Statement stmt;
		
		
		// db check for old admin
		try {
			stmt = db.conn.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT * FROM USERS");
			
			while(rs.next()) {
				if (rs.getInt(4) == 3) {
					first = false;
					adminName = rs.getString(2);
					break;
				}
			}
		} catch (SQLException e) {
			System.out.println("SQL Exception in Initial.java");
			e.printStackTrace();
		}
		
		// main bank object
		Bank b = null;
		
		if (first) {
			b = new Bank();
			} else {
				b = new Bank(adminName);
			}
		System.out.println("--> Thank you for visiting our bank today. Have a good day!");
	}
}
