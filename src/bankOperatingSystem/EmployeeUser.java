package bankOperatingSystem;

import java.util.Scanner;

public class EmployeeUser extends User {
	
	public EmployeeUser(Integer phone, String name, String password) {
		super(phone, name, password);
	}
	
	@Override
	public void menu() {
		Boolean exit = false;
		
		while (!exit) {
		
		Scanner scan = new Scanner(System.in);

		Integer userValue = 0;
		
		while (userValue == 0) {
			
			System.out.println("<-------------------->");
			System.out.println("    EMPLOYEE MENU");
			System.out.println("<-------------------->");
			System.out.println("-> Welcome "+ name + "!");
			System.out.println("What would you like to do today?");
			
			System.out.println("1. Display User Information");
			System.out.println("2. Display User Transactions");
			System.out.println("3. Display Account Information");
			System.out.println("4. View Bank Transactions");
			System.out.println("5. Appove/Deny Accounts");
			System.out.println("6. Exit");
			
			try {
				
				userValue = scan.nextInt();
				
			} catch (Exception e) {
				System.out.println("Sorry, I didn't get that.");
			}
			
			switch(userValue) {
			case 1:
				displayUserInfo(scan);
				userValue = 0;
				break;
			case 2: 
				displayUserTransactions(scan);
				userValue = 0;
				break;
			case 3:
				displayAccountInfo(scan);
				break;
			case 4:
				approveDeny(scan);
				userValue = 0;
				break;
			case 5:
				viewGlobalTransacations();
				break;
			case 6:
				exit = true;
				break;
			case 7:
				return;
			} // end switch
		} // end while
		} // end while
	} // end method
	
	public void displayUserInfo(Scanner scan){
		
		System.out.println("Enter the phone number of the user you would like to view.");
		
		Integer userChoice = scan.nextInt();
		
		UserOracleDAO db = new UserOracleDAO();
		db.getUserInfo(userChoice); 
		
	}
	
	private void displayUserTransactions(Scanner scan) {
		
		System.out.println("Please enter the phone number of the user:");
		
		Integer userChoice = scan.nextInt();
		
		UserOracleDAO db = new UserOracleDAO();
		
		db.userTransactions(userChoice);
	}

	public void displayAccountInfo(Scanner scan) {
		
		System.out.println("Which account would you like to view?");
		
		Integer userChoice = scan.nextInt();
		
		AccountOracleDAO db = new AccountOracleDAO();
		
		db.displayAccountInfo(userChoice);
		
	}
	
	public void approveDeny(Scanner scan) {
		System.out.println("Which account would you like to approve?");
		
		Integer userChoice = scan.nextInt();
		
		AccountOracleDAO db = new AccountOracleDAO();
		
		db.approveAccount(userChoice);
		
		TransactionOracleDAO db_log = new TransactionOracleDAO();
		
		db_log.approveAccountTransaction(phone, userChoice);
		
		System.out.println("--> Account Approved");	
		
	}
	
	public void viewGlobalTransacations() {
		UserOracleDAO db = new UserOracleDAO();
		db.globalTransactions();
		
	}
}

