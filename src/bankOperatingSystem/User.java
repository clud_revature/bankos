package bankOperatingSystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class User implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String name;
	public String password;
	public Integer phone;
	
	// constructor
	public User(Integer phone, String name, String password) {
		this.name = name;
		this.password = password;	
		this.phone = phone;
	}
	
	/// main menu
	public void menu() {		
		Boolean exit = false;
		
		while (!exit) {
		
		System.out.println("<-------------------->");
		System.out.println("    CUSTOMER MENU");
		System.out.println("<-------------------->");
		System.out.println("-> Welcome "+ name + "!");
		System.out.println("What would you like to do today?");
		
		Scanner scan = new Scanner(System.in);

		Integer userValue = 0;
		
		while (userValue == 0) {
						
			System.out.println("1. Display Account Information");
			System.out.println("2. Apply for a New Account");
			System.out.println("3. Apply for a New Joint Account.");
			System.out.println("4. Deposit funds");
			System.out.println("5. Withdraw funds");
			System.out.println("6. Transfer funds");
			System.out.println("7. Display Transaction History.");
			System.out.println("8. Logout");
			
			try {
				userValue = scan.nextInt();
			} catch (Exception e) {
				System.out.println("Sorry, I didn't get that.");
			}
			
			switch(userValue) {
			case 1:
				displayInfo(scan);
				userValue = 0;
				break;
			case 2: 
				apply(scan);
				break;
			case 3:
				applyJoint(scan);
				break;
			case 4:
				deposit(scan);
				break;
			case 5:
				withdraw(scan);
				break;
			case 6:
				transfer(scan);
				break;
			case 7:
				viewTransactions(scan);
			case 8:
				exit = true;
			} // end swithc
		} // end while
		} // end while
	} // end method
	
	
	public void displayInfo(Scanner scan) {
		System.out.println("<-------------------->");
		System.out.println("YOUR ACCOUNT INFORMATION");
		System.out.println("<-------------------->");
		System.out.println("Here is your information");
		System.out.println("Your name is "+name+".");
		
		AccountOracleDAO db = new AccountOracleDAO();
		db.displayUserAccountInfo(phone);
		
		System.out.println("<-------------------->");
	}
	
	public void apply(Scanner scan) {
		
		System.out.println("Let us set up your account today!");
		
		Double balance = (double) 0;
		
		System.out.println("How much would you like to deposit to open your account?");
		balance = scan.nextDouble();
		
		// db
		AccountOracleDAO db = new AccountOracleDAO();
		db.createAccount(phone, balance);
		
		// db transaction log
		TransactionOracleDAO db_log = new TransactionOracleDAO();
		db_log.createAccountTransaction(phone);
		
		System.out.println("--> Account created!");
	}
	
public void applyJoint(Scanner scan) {
		
		System.out.println("Let us set up your joint account today!");
		
		UserOracleDAO db_us = new UserOracleDAO();
		
		AccountOracleDAO db_acc = new AccountOracleDAO();
		
		System.out.println("Enter the phone number of the user you would like to share an account: ");
		
		Integer jointUserNum = scan.nextInt();
		
		System.out.println("Please verify by entering their password: ");
		
		String jointPass = scan.next();
		
		if (db_us.verifyUser(jointUserNum, jointPass)) {
			System.out.println("Good pass");
			
			Double balance = (double) 0;
			
			System.out.println("How much would you like to deposit to open your account?");
			balance = scan.nextDouble();
			
			db_acc.createJointAccount(phone, jointUserNum, balance);
			
			TransactionOracleDAO db_tr = new TransactionOracleDAO();
			
			db_tr.createJointAccountTransaction(phone, jointUserNum);
			
			db_tr.createJointAccountTransaction(jointUserNum, phone);
			
		} else {
			System.out.println("Bad pass");
		}
	}

	public void deposit(Scanner scan) {
		
		Integer userChoice = 0;
		double amount = 0;
		
		while (userChoice == 0) {
			
			System.out.println("Which account would you like deposit to?");
			
			// shows options
			AccountOracleDAO db = new AccountOracleDAO();
			db.displayUserAccountInfo(phone);
		
			userChoice = scan.nextInt();
			
			// verify ownership
			if (!db.verifyAccountOwnershp(userChoice, phone)) {
				System.out.println("--> Sorry, you do not have access to that account.");
				return;
			}
			
			// check approved status
			if (!db.checkApproved(userChoice)) {
				System.out.println("--> Sorry, this account is not approved for transactions.");
				return;
			}
			
			System.out.println("How much would you like to deposit?");
			amount = scan.nextDouble();
			
			// verify positive
			if (amount < 0) {
				System.out.println("Sorry, please enter a positive amount.");
				break;
			}
			
			// db
			Double currentBal = db.getAccountBalance(userChoice);
			db.changeBalance(userChoice, amount + currentBal);
			
			// db log
			TransactionOracleDAO db_log = new TransactionOracleDAO();
			db_log.changeBalanceTransaction(phone, userChoice, currentBal, currentBal + amount);
			
			System.out.println("--> Deposited funds.");
			
		}
	}
	
	public void withdraw(Scanner scan) {
		
		Integer userChoice = 0;
		Double amount;
		
		while (userChoice == 0) {
			
			System.out.println("Which account would you like to withdraw from?");
			
			// shows options
			AccountOracleDAO db = new AccountOracleDAO();
			db.displayUserAccountInfo(phone);
			
			userChoice = scan.nextInt();
			
			// verify ownership
			if (!db.verifyAccountOwnershp(userChoice, phone)) {
				System.out.println("--> Sorry, you do not have access to that account.");
				return;
			}
			
			// check approved status
			if (!db.checkApproved(userChoice)) {
				System.out.println("--> Sorry, this account is not approved for transactions.");
				return;
			}
			
			System.out.println("How much would you like to withdraw?");
			amount = scan.nextDouble();
			
			// verify positive
			if (amount < 0) {
				System.out.println("Sorry, please enter a positive amount.");
				break;
			}
						
			Double currentBal = db.getAccountBalance(userChoice);
			
			// verify balance
			if(currentBal < amount) {
				System.out.println("--> Sorry, you do not have the available funds for that withdraw.");
				userChoice=0;
				break;
			}
				
			// db
			db.changeBalance(userChoice, currentBal - amount);
			
			// db log
			TransactionOracleDAO db_log = new TransactionOracleDAO();
			db_log.changeBalanceTransaction(phone, userChoice, currentBal, currentBal - amount);
			
			// gimmick
			System.out.println("Here is your money: ");
			Integer gimmickCount = 0;
			
			for (int i = 0; i < amount; i ++) {
				System.out.print("|$|");
				gimmickCount++;
				if (gimmickCount == 10) {
					gimmickCount = 0;
					System.out.println("");
				}
			}
			
			System.out.println("--> Withdrew funds.");
		}
	}
	
	public void transfer(Scanner scan) {
		
		Integer userChoice = 0;
		Double amount;
		
		while (userChoice == 0) {
			
			System.out.println("Which account would you like to transfer from?");
			
			// show options
			AccountOracleDAO db = new AccountOracleDAO();
			db.displayUserAccountInfo(phone);
			
			userChoice = scan.nextInt();
			
			// verify ownership
			if (!db.verifyAccountOwnershp(userChoice, phone)) {
				System.out.println("--> Sorry, you do not have access to that account.");
				return;
			}
			
			// check approved status
			if (!db.checkApproved(userChoice)) {
				System.out.println("--> Sorry, this account is not approved for transactions.");
				return;
			}
			
			System.out.println("How much would you like to transfer?");
			amount = scan.nextDouble();
			
			// verify positive
			if (amount < 0) {
				System.out.println("Sorry, please enter a positive amount.");
				break;
			}
			
			// verify funds
			Double currentBal = db.getAccountBalance(userChoice);
			if (currentBal < amount) {
				System.out.println("--> Sorry, you do not have the available funds for that withdraw.");
				userChoice=0;
				break;
			}
			
			System.out.println("Which account would you like to transfer to?");
			Integer destChoice = scan.nextInt();
			
			// check approved status
			if (!db.checkApproved(destChoice)) {
				System.out.println("--> Sorry, this account is not approved for transactions.");
				return;
			}
			
			// db
			Double destBal = db.getAccountBalance(destChoice);
			db.changeBalance(userChoice, currentBal - amount);
			db.changeBalance(destChoice, destBal + amount);
			
			// db transaction log
			TransactionOracleDAO db_log = new TransactionOracleDAO();
			db_log.changeBalanceTransaction(phone, userChoice, currentBal, currentBal - amount);
			db_log.changeBalanceTransaction(phone, destChoice, destBal, destBal + amount);
			
			System.out.println("--> Transferred funds.");	
		}
	}
	
	private void viewTransactions(Scanner scan) {
		
		UserOracleDAO db = new UserOracleDAO();
		db.userTransactions(phone);
		
	}
}
