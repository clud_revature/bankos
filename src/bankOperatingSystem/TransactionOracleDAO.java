package bankOperatingSystem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

public class TransactionOracleDAO implements TransactionDAO {
	
	private Connection conn = JDBCConnection.getInstance().conn;

	@Override
	public void createUserTransaction(Integer userPhone) {
		String sql = "INSERT INTO TRANSACTIONS (USERNUM, STRING) VALUES (?, ?)";
		
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, userPhone);
			ps.setString(2, "User created!");
			
			ps.execute();
			
		} catch (SQLIntegrityConstraintViolationException e) {
			System.out.println("Sorry, phone number already exists. Please login or register again!");
		} catch (SQLException e) {
			System.out.println("Error in TransactionOracleDAO: createUserTransaction");
			e.printStackTrace();
		}
		
	}

	@Override
	public void createAccountTransaction(Integer userNo) {
		
		String sql = "INSERT INTO TRANSACTIONS (USERNUM, STRING) VALUES (?, ?)";
		
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, userNo);
			ps.setString(2, "Account created!");
			
			ps.execute();
			
		} catch (SQLException e) {
			System.out.println("Error in TransactionOracleDAO: createAccountTransaction");
			e.printStackTrace();
		}
	}

	@Override
	public void createJointAccountTransaction(Integer userNo1, Integer userNo2) {
		
		String sql = "INSERT INTO TRANSACTIONS (USERNUM, STRING) VALUES (?, ?)";
		
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, userNo1);
			ps.setString(2, "Joint Account created with:  " + userNo2);
			
			ps.execute();
			
		} catch (SQLException e) {
			System.out.println("Error in TransactionOracleDAO: createJointAccountTransaction");
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void changeBalanceTransaction(Integer userNo, Integer accNo, Double oldBal, Double newBal) {
		
		String sql = "INSERT INTO TRANSACTIONS (USERNUM, ACCOUNT, STRING) VALUES (?, ?, ?)";
		
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, userNo);
			ps.setInt(2, accNo);
			ps.setString(3, "Account balance changed: Old Balance: "+ oldBal + "--> New Balance: " + newBal);
			
			ps.execute();
			
		} catch (SQLException e) {
			System.out.println("Error in TransactionOracleDAO: approveAccountTransaction");
			e.printStackTrace();
		}
	}

	@Override
	public void approveAccountTransaction(Integer userNo, Integer accNo) {
		
		String sql = "INSERT INTO TRANSACTIONS (USERNUM, ACCOUNT, STRING) VALUES (?, ?, ?)";
		
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, userNo);
			ps.setInt(2, accNo);
			ps.setString(3, "Employee approved account.");
			
			ps.execute();
			
		} catch (SQLException e) {
			System.out.println("Error in TransactionOracleDAO: approveAccountTransaction");
			e.printStackTrace();
		}	
	}
}
