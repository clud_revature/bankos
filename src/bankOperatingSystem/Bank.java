package bankOperatingSystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Bank {
	
	Scanner scan = new Scanner(System.in);
	
	// main constructor when no previous installation found
	public Bank() {		
		// creation of new bank
		System.out.println("--> Creating a new bank.");
		System.out.println("--> First, create an administrator account.");
		// starts with creation of administrator (type = 3)
		register(3);
		// drops to main menu
		teller();
	}
	
	// constructor when previous installation found
	public Bank(String adminName){
		System.out.println("Using previous installation.");
		System.out.println("Admin Name: "+ adminName);
		teller();
	}
	
	// main menu (teller)
	public void teller(){
		
		// exit loop
		boolean exit = false;
		while (!exit) {
									
			Integer userValue = 0;	
			
			while (userValue == 0) {
				
				System.out.println("<-------------------->");
				System.out.println("      BANK MENU");
				System.out.println("<-------------------->");
				System.out.println("Welcome to the Bank!");
				System.out.println("What would you like to do today?");
				System.out.println("1. Login");
				System.out.println("2. Register as customer.");
				System.out.println("3. Register as employee.");
				System.out.println("4. Exit");
				
				try {
					userValue = scan.nextInt();
					} catch (Exception e) {
					// if bad input, will throw error and reset
					System.out.println("Sorry, I didn't get that.");
					scan.nextLine();
					}
												
				switch(userValue) {
				case 1:
					login();
					break;
				case 2: 
					register(1);
					break;
				case 3:
					register(2);
					break;
				case 4:
					exit = true;
					scan.close();
					break;
				// if no good value, runs here
				default:
					System.out.println("Please enter a correct value and try again.");
					userValue = 0;
					break;
				}
			}
		}		
	}
	
	// login method: portal to other menus
	private void login() {
		
		System.out.println("--> Please enter your phone number:");
		Integer phone;
		
		try {
			phone = scan.nextInt();
		} catch (Exception e) {
			System.out.println("Bad input? Please try again with a numeric phone number with no spaces.");
			scan.nextLine();
			return;
		}
		
		scan.nextLine();
		System.out.println("--> Please enter your password:");
		String password = scan.nextLine();
		
		UserOracleDAO db = new UserOracleDAO();
		User us = null;
		
		try {
			us = db.getUserByPhone(phone, password);
			us.menu();
		} catch (InvalidPasswordException e) {
			System.out.println("--> Sorry, invalid password. Please try again!");
		}
	}

	private void register(Integer type) {
		
		// user variables to be filled
		String name = null;
		String password = null;
		Integer phone = null;
		// type (access level) assigned by previous menu
		Integer accountType = type;
		
		while (!(name != null)) {
			System.out.println("Please enter your name.");
			
			// fixing flow problem here
			if (type == 1 || type == 2) {
				scan.nextLine();
			}
			//
			
			try {
				name = scan.nextLine();
			} catch (Exception e) {
				System.out.println("Sorry, bad input, please enter an alphanumeric name.");
			}
		}
				
		while (phone == null) {
			System.out.println("Please enter your phone.");
		try {
			phone = scan.nextInt();
		} catch (Exception e) {
			System.out.println("Sorry, bad input, please enter an numeric phone with no spaces.");
			scan.nextLine();
			}
		}	
		
		while (password == null) {
			System.out.println("Please enter your password.");
		try {
			scan.nextLine();
			password = scan.nextLine();
		} catch (Exception e) {
			System.out.println("Sorry, bad input, please enter a text password.");
			}
		}
		
		// db writing
		UserOracleDAO db = new UserOracleDAO();
		db.createUser(phone, name, password, type);
		
		// transaction logging
		TransactionOracleDAO db_log = new TransactionOracleDAO();
		db_log.createUserTransaction(phone);
	}

}
