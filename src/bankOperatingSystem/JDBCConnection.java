package bankOperatingSystem;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {
	
	final String user = "admin";
	final String password = "12345678";
	final String db_url = "jdbc:oracle:thin:@java-react-db1.cjtcgekhfabw.us-east-2.rds.amazonaws.com:1521:ORCL";
	
	private static JDBCConnection single_instance = null;
	public Connection conn;
	
	
	public JDBCConnection() {
		
		try {
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			this.conn = DriverManager.getConnection(db_url, user, password);

		} catch (ClassNotFoundException e) {
			System.out.println("ClassNotFound Exception..."+e);
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("SQL Exception..."+e);
			e.printStackTrace();
		}
		// if it gets to this point...
		System.out.println("--> Connection established.");
	}
	
	public static JDBCConnection getInstance() {
		if (single_instance==null) {
			single_instance = new JDBCConnection();
		} 
		return single_instance;
	}
}
