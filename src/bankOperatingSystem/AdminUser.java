package bankOperatingSystem;

import java.util.Scanner;

public class AdminUser extends EmployeeUser {

	public AdminUser(Integer phone, String name, String password) {
		super(phone, name, password);
}
	
	@Override
	public void menu() {
		
		Boolean exit = false;
		
		while (!exit) {
		
		Scanner scan = new Scanner(System.in);

		Integer userValue = 0;
		
		while (userValue == 0) {
			
			System.out.println("<-------------------->");
			System.out.println("     ADMIN MENU");
			System.out.println("<-------------------->");
			System.out.println("Hello "+name+"!");
			System.out.println("What would you like to do today?");
			
			System.out.println("1. Display User Information");
			System.out.println("2. Display Account Information");
			System.out.println("3. Appove/Deny Accounts");
			System.out.println("4. Withdraw Funds");
			System.out.println("5. Deposit Funds");
			System.out.println("6. Transfer Funds");
			System.out.println("7. Cancel Accounts");
			System.out.println("8. View Global Bank Transactions.");
			System.out.println("9. Exit");
			
			try {
				userValue = scan.nextInt();
				
			} catch (Exception e) {
				System.out.println("Sorry, I didn't get that.");
			}
			
			switch(userValue) {
			case 1:
				displayUserInfo(scan);
				userValue = 0;
				break;
			case 2: 
				displayAccountInfo(scan);
				userValue = 0;
				break;
			case 3:
				approveDeny(scan);
				userValue = 0;
				break;
			case 4:
				withdrawFunds(scan);
				userValue = 0;
				break;
			case 5:
				depositFunds(scan);
				userValue = 0;
				break;
			case 6:
				transferFunds(scan);
				userValue = 0;
				break;
			case 7:
				deleteAccounts(scan);
				break;
			case 8:
				viewGlobalTransacations();
				break;
			case 9:
				exit = true;
				break;
				} // end switch
			} // end internal while
		} // end while
	}	 // end method
	
	private void transferFunds(Scanner scan) {
		
		Integer userChoice;
		Double amount;
		
		System.out.println("Which account would you like to transfer from?");
		
		userChoice = scan.nextInt();
		
		AccountOracleDAO db = new AccountOracleDAO();
		
		// check approved status
		if (!db.checkApproved(userChoice)) {
			System.out.println("--> Sorry, this account is not approved for transactions.");
			return;
		}
		
		System.out.println("How much would you like to transfer?");
		
		amount = scan.nextDouble();
					
		Double currentBal = db.getAccountBalance(userChoice);
		
		// verify positive
		if (amount < 0) {
			System.out.println("Sorry, please enter a positive amount.");
			return;
		}
		
		// verify funds
		if (currentBal < amount) {
			System.out.println("--> Sorry, not enough available funds for that withdraw.");
			return;
		}
		
		System.out.println("Which account would you like to transfer to?");
		
		Integer destChoice = scan.nextInt();
		
		// check approved status
		if (!db.checkApproved(destChoice)) {
			System.out.println("--> Sorry, this account is not approved for transactions.");
			return;
		}
		
		Double destBal = db.getAccountBalance(destChoice);
		
		db.changeBalance(userChoice, currentBal - amount);
		
		db.changeBalance(destChoice, destBal + amount);
		
		TransactionOracleDAO db_log = new TransactionOracleDAO();
		
		db_log.changeBalanceTransaction(phone, userChoice, currentBal, currentBal - amount);
		
		db_log.changeBalanceTransaction(phone, destChoice, destBal, destBal + amount);
		
		System.out.println("--> Transferred funds.");
		
	}


	private void depositFunds(Scanner scan) {
		
		Integer userChoice = 0;
		double amount = 0;
		
		while (userChoice == 0) {
			
			System.out.println("Which account would you like deposit to?");
			
			AccountOracleDAO db = new AccountOracleDAO();
			
			userChoice = scan.nextInt();
			
			// check approved status
			if (!db.checkApproved(userChoice)) {
				System.out.println("--> Sorry, this account is not approved for transactions.");
				return;
			}
			
			System.out.println("How much would you like to deposit?");
				
			amount = scan.nextDouble();
			
			// verify positive
			
			if (amount < 0) {
				System.out.println("Sorry, please enter a positive amount.");
				break;
			}
						
			Double currentBal = db.getAccountBalance(userChoice);
				
			db.changeBalance(userChoice, amount + currentBal);
			
			TransactionOracleDAO db_log = new TransactionOracleDAO();
			
			db_log.changeBalanceTransaction(phone, userChoice, currentBal, currentBal + amount);
			
			System.out.println("--> Deposited funds.");
		}
		
	}

	public void withdrawFunds(Scanner scan) {
		Integer userChoice = 0;
		Double amount;
		
		while (userChoice == 0) {
			
			System.out.println("Which account would you like to withdraw from?");
			
			AccountOracleDAO db = new AccountOracleDAO();
			
			userChoice = scan.nextInt();
			
			// check approved status
			if (!db.checkApproved(userChoice)) {
				System.out.println("--> Sorry, this account is not approved for transactions.");
				return;
			}
			
			System.out.println("How much would you like to withdraw?");
				
			amount = scan.nextDouble();
			
			// verify positive
			
			if (amount < 0) {
				System.out.println("Sorry, please enter a positive amount.");
				break;
			}
						
			Double currentBal = db.getAccountBalance(userChoice);
			
			if( currentBal < amount ) {
				System.out.println("--> Sorry, not enough available funds for that withdraw.");
				break;
			}
				
			db.changeBalance(userChoice, currentBal - amount);
			
			TransactionOracleDAO db_log = new TransactionOracleDAO();
			
			db_log.changeBalanceTransaction(phone, userChoice, currentBal, currentBal - amount);
			
			System.out.println("--> Withdrew funds.");
		}
	}
	
	private void deleteAccounts(Scanner scan) {
		
		System.out.println("Which account would you like to cancel?");
		
		Integer userChoice;
		
		userChoice = scan.nextInt();
		
		UserOracleDAO db = new UserOracleDAO();
		
		db.dropAccount(userChoice);
		
		System.out.println("Account is no longer with the bank");
	}

}
