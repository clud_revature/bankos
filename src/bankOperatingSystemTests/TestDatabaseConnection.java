package bankOperatingSystemTests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert.*;

import bankOperatingSystem.JDBCConnection;


import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Assert;

public class TestDatabaseConnection {
	
	private static Connection conn;
	
	@BeforeClass
	public static void setupBeforeClass() throws Exception {
		JDBCConnection db = new JDBCConnection();
		conn = JDBCConnection.getInstance().conn;

	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("Finished testing.");
		
	}
	
	@Before
	public void setUp() throws Exception {
		System.out.println("Testing...");
	}
	
	@After
	public void tearDown() throws Exception {
		System.out.println("...Finished Test.");
		
	}
	
	@Test
	public void testReadUsers() throws SQLException {
		System.out.println("Testing for reading from user table.");
		
		Statement stmt;
		
		stmt = conn.createStatement();
			
		ResultSet rs = stmt.executeQuery("SELECT * FROM USERS");

	}
	
	@Test
	public void testReadAccounts() throws SQLException {
		System.out.println("Testing for reading from accounts table.");
		
		Statement stmt;
		
			stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT * FROM ACCOUNTS");
			
	}
	
	@Test
	public void testReadTransactions() throws SQLException {
		System.out.println("Testing for reading from transactions table.");
		
		Statement stmt;
		
		stmt = conn.createStatement();
			
		ResultSet rs = stmt.executeQuery("SELECT * FROM TRANSACTIONS");
	}
}
